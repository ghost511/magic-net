﻿using Magic.Core;
using Magic.FlowCenter.Entity;
using System.Collections.Generic;

namespace Magic.FlowCenter.Service;

#region 输入参数
public class QueryFlcFlowinstancePageInput : PageParamBase
{
    /// <summary>
    /// Code
    /// </summary>
    public virtual string Code { get; set; }

    /// <summary>
    /// CustomName
    /// </summary>
    public virtual string CustomName { get; set; }

    /// <summary>
    /// ActivityId
    /// </summary>
    public virtual string ActivityId { get; set; }

    /// <summary>
    /// ActivityName
    /// </summary>
    public virtual string ActivityName { get; set; }

    /// <summary>
    /// PreviousId
    /// </summary>
    public virtual string PreviousId { get; set; }

    public virtual int LookType { get; set; }
}

#endregion

#region 输出参数
/// <summary>
/// 工作流输出参数
/// </summary>
public class FlcFlowinstanceOutput : FlcFlowinstance
{
    public string WebId { get; set; }
    public List<FlcFlowInstanceOperationHistory> hisList { get; set; }

}

#endregion