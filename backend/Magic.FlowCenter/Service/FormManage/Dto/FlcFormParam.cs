﻿using Magic.Core;
using SqlSugar;
using System.ComponentModel.DataAnnotations;

namespace Magic.FlowCenter.Service;

public class QueryFlcFormPageInput : PageParamBase
{
    public long Id { get; set; }
    public string Name { get; set; }
    public long OrgId { get; set; }
}

public class AddFlcFormInput
{
    /// <summary>
    /// 名称
    /// </summary>
    [Required(ErrorMessage = "表单名称不能为空")]
    public string Name { get; set; }

    /// <summary>
    /// 机构Id
    /// </summary>
    public long OrgId { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    public int Sort { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    public string Remark { get; set; }

    public FormType FrmType { get; set; }

    public string WebId { get; set; }

    /// <summary>
    ///  字段数
    /// 默认值: 
    ///</summary>
    public int? Fields { get; set; }

    /// <summary>
    ///  字段
    /// 默认值: 
    ///</summary>
    public string ContentData { get; set; }
    /// <summary>
    ///  字段格式化
    /// 默认值: 
    ///</summary>
    public string ContentParse { get; set; }
    /// <summary>
    ///  表单内容
    /// 默认值: 
    ///</summary>
    public string Content { get; set; }
}

public class EditFlcFormInput : AddFlcFormInput
{
    public long Id { get; set; }
}
