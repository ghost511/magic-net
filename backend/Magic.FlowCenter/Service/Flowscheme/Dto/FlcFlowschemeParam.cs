﻿using Magic.Core;
using SqlSugar;
using System.Diagnostics.Eventing.Reader;

namespace Magic.FlowCenter.Service;

#region 输入参数
public class QueryFlcFlowschemePageInput : PageParamBase
{
    public long Id { get; set; }
    public string Name { get; set; }
    public long OrgId { get; set; }

    public CommonStatus Status { get; set; }
}

public class AddFlcFlowschemeInput
{
    public string FrmId { get; set; }

    public long OrgId { get; set; }

    public string SchemeCode { get; set; }
    public string SchemeName { get; set; }
    public int Sort { get; set; }
    public string Remark { get; set; }

}

public class EditFlcFlowschemeInput : AddFlcFlowschemeInput
{
    public long Id { get; set; }

    public CommonStatus? Status { get; set; }

    /// <summary>
    ///  流程内容
    /// 默认值: 
    ///</summary>
    public string SchemeContent { get; set; }

    public string ContentParse { get; set; }
}
#endregion

#region 输出参数
/// <summary>
/// 流程管理输出参数
/// </summary>
public class FlcFlowschemeOutput
{
    /// <summary>
    /// 流程Id
    /// </summary>
    public string Id { get; set; }
    public int? Fields { get; set; }
    public string Name { get; set; }
    public string WebId { get; set; }
    public string ContentData { get; set; }
    public string ContentParse { get; set; }
    public string Content { get; set; }
    /// <summary>
    /// 如果下个执行节点是运行时指定执行者。需要传指定的类型
    /// <para>取值为RUNTIME_SPECIAL_ROLE、RUNTIME_SPECIAL_USER</para>
    /// </summary>
    public string NextNodeDesignateType { get; set; }

    /// <summary>
    /// 如果下个执行节点是运行时指定执行者。该值表示具体的执行者
    /// <para>如果NodeDesignateType为RUNTIME_SPECIAL_ROLE，则该值为指定的角色</para>
    /// <para>如果NodeDesignateType为RUNTIME_SPECIAL_USER，则该值为指定的用户</para>
    /// </summary>
    public string[] NextNodeDesignates { get; set; }
    public string NextMakerName { get; set; }

    public string SchemeCode { get; set; }
    public string SchemeName { get; set; }
    public string SchemeType { get; set; }
    public string SchemeVersion { get; set; }
    public string SchemeCanUser { get; set; }
    public string SchemeContent { get; set; }
    public string FrmId { get; set; }
    public FormType? FrmType { get; set; }
    public int? AuthorizeType { get; set; }
    public string OrgId { get; set; }
    public string Active { get; set; }
    public CommonStatus? Status { get; set; }
    public int? Sort { get; set; }
    public string Remark { get; set; }
}
#endregion