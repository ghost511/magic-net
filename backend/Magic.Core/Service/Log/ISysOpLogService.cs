﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysOpLogService : ITransient
{
    Task Clear();
    Task<PageList<SysLogOp>> PageList(QueryOpLogPageInput input);
}
