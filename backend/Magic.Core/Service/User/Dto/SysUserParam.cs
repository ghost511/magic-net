﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Magic.Core.Service;


#region 输入参数
public class QueryUserPageInput : PageParamBase
{
    /// <summary>
    /// 员工信息
    /// </summary>
    public EmpOutput SysEmpParam { get; set; } = new EmpOutput();

    /// <summary>
    /// 搜索状态（字典 0正常 1停用 2删除）
    /// </summary>
    public CommonStatus SearchStatus { get; set; }

    public string Name { get; set; }
}

public class AddUserInput
{
    /// <summary>
    /// 账号
    /// </summary>
    [Required(ErrorMessage = "账号名称不能为空")]
    public string Account { get; set; }

    /// <summary>
    /// 姓名
    /// </summary>
    [Required(ErrorMessage = "姓名不能为空")]
    public string Name { get; set; }

    /// <summary>
    /// 密码
    /// </summary>
    [Required(ErrorMessage = "密码不能为空")]
    public string Password { get; set; }

    /// <summary>
    /// 确认密码
    /// </summary>
    [Required(ErrorMessage = "确认密码不能为空"), Compare(nameof(Password), ErrorMessage = "两次密码不一致")]
    public string Confirm { get; set; }

    /// <summary>
    /// 昵称
    /// </summary>
    public string NickName { get; set; }

    /// <summary>
    /// 生日
    /// </summary>
    public DateTime? Birthday { get; set; }

    /// <summary>
    /// 性别
    /// </summary>
    public Gender Sex { get; set; }

    /// <summary>
    /// 邮箱
    /// </summary>
    public string Email { get; set; }

    /// <summary>
    /// 手机
    /// </summary>
    [Required(ErrorMessage = "手机不能为空")]
    public string Phone { get; set; }

    /// <summary>
    /// 电话
    /// </summary>
    public string Tel { get; set; }

    /// <summary>
    /// 员工信息
    /// </summary>
    public EmpOutput SysEmpParam { get; set; } = new EmpOutput();

}

public class EditUserInput
{
    public long Id { get; set; }
    /// <summary>
    /// 账号
    /// </summary>
    [Required(ErrorMessage = "账号名称不能为空")]
    public string Account { get; set; }

    /// <summary>
    /// 姓名
    /// </summary>
    [Required(ErrorMessage = "姓名不能为空")]
    public string Name { get; set; }

    /// <summary>
    /// 昵称
    /// </summary>
    public string NickName { get; set; }

    /// <summary>
    /// 生日
    /// </summary>
    public DateTime? Birthday { get; set; }

    /// <summary>
    /// 性别
    /// </summary>
    public Gender Sex { get; set; }

    /// <summary>
    /// 邮箱
    /// </summary>
    public string Email { get; set; }

    /// <summary>
    /// 手机
    /// </summary>
    [Required(ErrorMessage = "手机不能为空")]
    public string Phone { get; set; }

    /// <summary>
    /// 电话
    /// </summary>
    public string Tel { get; set; }

    /// <summary>
    /// 员工信息
    /// </summary>
    public EmpOutput SysEmpParam { get; set; } = new EmpOutput();
}

public class UpdateUserInfo
{
    public long Id { get; set; }
    /// <summary>
    /// 昵称
    /// </summary>
    public string NickName { get; set; }

    /// <summary>
    /// 生日
    /// </summary>
    [Required(ErrorMessage = "生日不能为空")]
    public DateTime? Birthday { get; set; }

    /// <summary>
    /// 性别
    /// </summary>
    public Gender Sex { get; set; }

    /// <summary>
    /// 邮箱
    /// </summary>
    [Required(ErrorMessage = "邮箱不能为空")]
    public string Email { get; set; }

    /// <summary>
    /// 手机
    /// </summary>
    [Required(ErrorMessage = "手机不能为空")]
    public string Phone { get; set; }

    /// <summary>
    /// 电话
    /// </summary>
    public string Tel { get; set; }
}

public class ChangeUserStatusInput
{
    public long Id { get; set; }
    /// <summary>
    /// 搜索状态（字典 0正常 1停用 2删除）
    /// </summary>
    public CommonStatus Status { get; set; }
}

/// <summary>
/// 用户授权参数，菜单、数据、角色
/// </summary>
public class GrantUserInput : PermissionBaseInput
{
    /// <summary>
    /// 用户Id
    /// </summary>
    [Required(ErrorMessage = "用户Id不能为空")]
    public long Id { get; set; }
}


public class ChangePasswordUserInput
{
    /// <summary>
    /// 用户Id
    /// </summary>
    [Required(ErrorMessage = "用户Id不能为空")]
    public long Id { get; set; }

    /// <summary>
    /// 密码
    /// </summary>
    [Required(ErrorMessage = "旧密码不能为空")]
    public string Password { get; set; }

    /// <summary>
    /// 新密码
    /// </summary>
    [Required(ErrorMessage = "新密码不能为空")]
    [StringLength(32, MinimumLength = 5, ErrorMessage = "密码需要大于5个字符")]
    public string NewPassword { get; set; }

    /// <summary>
    /// 确认密码
    /// </summary>
    [Required(ErrorMessage = "确认密码不能为空"), Compare(nameof(NewPassword), ErrorMessage = "两次密码不一致")]
    public string Confirm { get; set; }
}

public class UploadAvatarInput
{
    /// <summary>
    /// 用户Id
    /// </summary>
    [Required(ErrorMessage = "用户Id不能为空")]
    public long Id { get; set; }

    /// <summary>
    /// 头像文件路径标识
    /// </summary>
    [Required(ErrorMessage = "头像文件路径标识不能为空")]
    public long Avatar { get; set; }
}


#endregion

#region 输出参数
/// <summary>
/// 用户参数
/// </summary>
public class UserOutput
{
    /// <summary>
    /// Id
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    /// 账号
    /// </summary>
    public string Account { get; set; }

    /// <summary>
    /// 昵称
    /// </summary>
    public string NickName { get; set; }

    /// <summary>
    /// 姓名
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 头像
    /// </summary>
    public string Avatar { get; set; }

    /// <summary>
    /// 生日
    /// </summary>
    public DateTime Birthday { get; set; }

    /// <summary>
    /// 性别-男_1、女_2
    /// </summary>
    public int Sex { get; set; }

    /// <summary>
    /// 邮箱
    /// </summary>
    public string Email { get; set; }

    /// <summary>
    /// 手机
    /// </summary>
    public string Phone { get; set; }

    /// <summary>
    /// 电话
    /// </summary>
    public string Tel { get; set; }

    /// <summary>
    /// 状态-正常_0、停用_1、删除_2
    /// </summary>
    public int Status { get; set; }

    /// <summary>
    /// 员工信息
    /// </summary>
    public EmpOutput SysEmpInfo { get; set; }

    /// <summary>
    /// 所属租户
    /// </summary>
    public long? TenantId { get; set; }

    public string TenantName { get; set; }

    public List<string> RoleList { get; set; }
}
#endregion