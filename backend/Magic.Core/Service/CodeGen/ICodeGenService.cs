﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ICodeGenService: ITransient
{
    Task Add(AddCodeGenInput input);
    Task Delete(List<Core.PrimaryKeyParam> inputs);
    Task<SysCodeGen> Get(Core.PrimaryKeyParam input);
    List<TableColumnOutput> GetColumnList(AddCodeGenInput input);
    Task<List<TableOutput>> GetTableList();
    Task<PageList<SysCodeGen>> PageList(QueryCodeGenPageInput input);
    Task RunLocal(SysCodeGen input);
    Task UpdateCodeGen(EditCodeGenInput input);

    List<TableColumnOutput> GetColumnListByTableName(string tableName);
}
