﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magic.Core.Service;

#region 输入参数
public class QueryOrgPageInput : PageParamBase
{
    /// <summary>
    /// Id
    /// </summary>
    public string Id { get; set; }

    /// <summary>
    /// 父Id
    /// </summary>
    public string Pid { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    public string Name { get; set; }
}

public class AddOrgInput
{
    /// <summary>
    /// 名称
    /// </summary>
    [Required(ErrorMessage = "机构名称不能为空")]
    public string Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    [Required(ErrorMessage = "机构编码不能为空")]
    public string Code { get; set; }

    /// <summary>
    /// 父Id
    /// </summary>
    [Required(ErrorMessage = "Pid不能为空")]
    public string Pid { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    public string Remark { get; set; }

    /// <summary>
    /// 状态（字典 0正常 1停用 2删除）
    /// </summary>
    public int Status { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    public int Sort { get; set; }
}

public class EditOrgInput : AddOrgInput
{
    public long Id { get; set; }
}

#endregion

#region 输出参数
/// <summary>
/// 组织机构参数
/// </summary>
public class OrgOutput
{
    /// <summary>
    /// 机构Id
    /// </summary>
    public string Id { get; set; }

    /// <summary>
    /// 父Id
    /// </summary>
    public string Pid { get; set; }

    /// <summary>
    /// 父Ids
    /// </summary>
    public string Pids { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    public virtual string Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    public virtual string Code { get; set; }

    /// <summary>
    /// 电话
    /// </summary>
    public virtual string Tel { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    public int Sort { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    public string Remark { get; set; }

    /// <summary>
    /// 状态（字典 0正常 1停用 2删除）
    /// </summary>
    public int Status { get; set; }
}
#endregion