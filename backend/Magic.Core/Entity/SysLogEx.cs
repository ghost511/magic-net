﻿
using SqlSugar;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace Magic.Core.Entity;

/// <summary>
/// 异常日志
/// </summary>
[SugarTable("sys_log_ex")]
[Description("异常日志")]
public class SysLogEx : AutoIncrementEntity
{
    /// <summary>
    /// 操作人
    /// </summary>
    [SugarColumn(ColumnDescription = "操作人", IsNullable = true,Length =32)]
    public string Account { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    [SugarColumn(ColumnDescription = "名称", IsNullable = true, Length = 32)]
    public string Name { get; set; }

    /// <summary>
    /// 类名
    /// </summary>
    [SugarColumn(ColumnDescription = "类名", IsNullable = true, Length = 255)]
    public string ClassName { get; set; }

    /// <summary>
    /// 方法名
    /// </summary>
    [MaxLength(255)]
    [SugarColumn(ColumnDescription = "方法名", IsNullable = true, Length = 255)]
    public string MethodName { get; set; }

    /// <summary>
    /// 异常名称
    /// </summary>
    [SugarColumn(ColumnDescription = "异常名称", IsNullable = true, Length = 255)]
    public string ExceptionName { get; set; }

    /// <summary>
    /// 异常信息
    /// </summary>
    [SugarColumn(ColumnDescription = "异常信息", IsNullable = true, Length = 255)]
    public string ExceptionMsg { get; set; }

    /// <summary>
    /// 异常源
    /// </summary>
    [SugarColumn(ColumnDescription = "异常源", IsNullable = true, Length = 255)]
    public string ExceptionSource { get; set; }

    /// <summary>
    /// 堆栈信息
    /// </summary>
    [SugarColumn(ColumnDescription = "堆栈信息", IsNullable = true, ColumnDataType = StaticConfig.CodeFirst_BigString)]
    public string StackTrace { get; set; }

    /// <summary>
    /// 参数对象
    /// </summary>
    [SugarColumn(ColumnDescription = "参数对象", IsNullable = true, ColumnDataType = StaticConfig.CodeFirst_BigString)]
    public string ParamsObj { get; set; }

    /// <summary>
    /// 异常时间
    /// </summary>
    [SugarColumn(ColumnDescription = "异常时间", IsNullable = true)]
    public DateTime ExceptionTime { get; set; }
}
