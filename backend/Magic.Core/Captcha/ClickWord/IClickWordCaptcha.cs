﻿namespace Magic.Core;

public interface IClickWordCaptcha
{
    CaptchaVerifyResult_back CheckCode(ClickWordCaptchaInput input);
    CaptchaVerifyResult_back CreateCaptchaImage(string code, int width, int height);
    string RandomCode(int number);
}
